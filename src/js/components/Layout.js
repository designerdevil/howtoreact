import React from "react";
import InputPlaceholder from "./extended/InputPlaceholder";

export default class Layout extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: "Defined Property",
		};
	}
	newTitleValue(title){
		if(title === "")
			title = "Defined Property"
		this.setState({
			title: title,
		});
	}
	render() {
		return (
		  <div>
		    <h1>{this.state.title}</h1>
		    <InputPlaceholder newTitleValue={this.newTitleValue.bind(this)} defineProperty={this.state.title} />
		  </div>
		);
	}
}
