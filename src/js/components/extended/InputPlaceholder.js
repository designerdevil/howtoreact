import React from "react";

export default class InputPlaceholder extends React.Component {
  changeTitle(e){
  	const newValue = e.target.value;
  	this.props.newTitleValue(newValue);
  }
  render() {
    return (
      <div>
        <h2>Extended Component</h2>
        <input placeholder={this.props.defineProperty} onChange={this.changeTitle.bind(this)} />
      </div>
    );
  }
}
